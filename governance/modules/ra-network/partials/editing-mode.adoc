// Per des. 2020, asciidoc editors like e.g. AsciidocFX still doesn't support Antora's way of addressing components and modules. Here, a few modifications is made to support editing mode
:imagepath: ../images/
:toc: left
:sectnums:
:toclevels: 5
:sectnumlevels: 7
