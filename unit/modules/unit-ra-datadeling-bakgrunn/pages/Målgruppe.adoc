= Målgruppe
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: master@drafts:unit-ra-datadeling-bakgrunn:
endif::[]
:toc: left
:toclevels: 3
:sectnums:
:sectnumlevels: 9

Målgruppen for referansearkitekturen er primært arkitekter og tekniske prosjektledere.

