= Få tilgang til data - løsningsmønster UHF
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagesdir: ../images
endif::[]
:toc: left
:toclevels: 7
:sectnums:
:sectnumlevels: 7

== Oversikt

Tjeneste for å registrere og vedlikeholde API-definisjoner og scopes i Maskinporten.

.Få tilgang til data - løsningsmønster UHF
image::Få tilgang til data - løsningsmønster UHF.png[alt=Få tilgang til data - løsningsmønster UHF image]

== Data-konsument

Den som innhenter eller mottar data fra andre aktører.

== Beskrivelse API

Dataobjekt som er en maskinlesbar beskrivelse av REST API-er iht. Open API Specification. Dette er formatet som benyttes for å registrere et API i felles API-katalog

Ønsker å bruke Open API spesifikasjon

== API-søk

Tjeneste for å søke etter og finne tilgjengelige API-er

== Få tilgang til data (konsument)

Evnen til å skaffe seg tilgang til tilbudte data fra annen aktør. 

== UHF API katalog

Registeret skal være mest mulig selvbetjent

== UHF tildel rettigheter til klienter



== API tilgangs-forespørsel

API eieren får forespørsel om den vil gi konsumenten tilgang til APIet.

== UHF Oauth2 autorisasjonstjener

Autorisasjonstjeneter realiserer tilgangskontroll gjennom å utstede OpenID Connect eller OAuth 2 grants og tokens. En autorisasjonstjener er også brukt til å realisere tilgangs policy. 

Scope (rettighet) og Audience (identifikator eller ressurs-server f. eks. FS)

Per i dag er Scopes er grovkornet
Rich authorization requsts (ny spesifikasjon) for å utvide scope begrep i IETF
underenhet og behandlingssted, behandlingsgrunnlag og roller jobbes det med.

=== UHF Klientregister



== OAUTH scopes

Hvordan får Dataobjekt som som kan beskrives som en ressurs-definisjon, og et token er som regel knyttet til ett eller flere scopes. Scopes benyttes til å styre tilganger til API-er og operasjoner, samt eventuelt hva slags responser man får fra API-er.

Fra eHelse: Regler for et API som beskriver hvilke tilganger en datakonsument (representert ved organisasjonsnummer) og deres klienter skal ha tilgang til (utstedt token for).

