= Dataobjekter eOppslag - med metadata UHF
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagesdir: ../images
endif::[]
:toc: left
:toclevels: 7
:sectnums:
:sectnumlevels: 7

== Oversikt



.Dataobjekter eOppslag - med metadata UHF
image::Dataobjekter eOppslag - med metadata UHF.png[alt=Dataobjekter eOppslag - med metadata UHF image]

== API-katalogen

API-katalogen er en del av Felles datakatalog som inneholder API-beskrivelser med endepunktsadresser og kobling til datasett.

== Tilganger konsument/representasjonsforhold

Dataobjekt som beskriver hvilke tilganger til ressurser en representant (leverandør) skal ha på vegne av konsument.

== Scope-metadata



== Samtykkemetadata



== API-katalogmetadata



== Maskinporten

Maskinporten sørger for sikker autentisering og tilgangskontroll for datautveksling mellom
virksomheter, maskin til maskin. Dette gir teknisk og organisatorisk samhandlingsevne.

Mer om Maskinporten:
https://www.digdir.no/digitale-felleslosninger/maskinporten/869

== Altinn autorisasjon

Altinn autorisasjon gir teknisk samhandlingsevne ved at bruker logger inn med elektronisk ID,
autorisasjonskomponenten sjekker at bruker har de tilgangene som kreves for å ta i bruk aktuell tjeneste.

Dersom en privatperson skal ta i bruk en tjeneste på vegne av en virksomhet, gjøres det et oppslag mot Enhetsregisteret for å bekrefte at personen har en rolle på vegne av virksomheten. Dette gir organisatorisk samhandlingsevne.

Mer om Altinn autorisasjon:
https://www.altinndigital.no/produkter/styring-av-tilgang/


== Beskrivelse API

Dataobjekt som er en maskinlesbar beskrivelse av REST API-er iht. Open API Specification. Dette er formatet som benyttes for å registrere et API i felles API-katalog

Ønsker å bruke Open API spesifikasjon

=== Delegerbar ressurs

Dataobjekt som beskriver en ressurs, f.eks. et API, som det kan gis rettigheter til gjennom et representasjonsforhold.

==== OAUTH scopes

Hvordan får Dataobjekt som som kan beskrives som en ressurs-definisjon, og et token er som regel knyttet til ett eller flere scopes. Scopes benyttes til å styre tilganger til API-er og operasjoner, samt eventuelt hva slags responser man får fra API-er.

Fra eHelse: Regler for et API som beskriver hvilke tilganger en datakonsument (representert ved organisasjonsnummer) og deres klienter skal ha tilgang til (utstedt token for).

=== Endepunkt (adresse)

Dataobjekt som representerer teknisk adresse til et API eller ressurs.

== Beskrivelse av aktør som kan innhente samtykke

Dataobjekt som beskriver en datakonsument som har rett til å innhente samtykke om å slå opp data.
Viktig innholde er bl.a. URL

