= Delegere rettigheter til databehandler - løsningsmønster UHF
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagesdir: ../images
endif::[]
:toc: left
:toclevels: 7
:sectnums:
:sectnumlevels: 7

== Oversikt



.Delegere rettigheter til databehandler - løsningsmønster UHF
image::Delegere rettigheter til databehandler - løsningsmønster UHF.png[alt=Delegere rettigheter til databehandler - løsningsmønster UHF image]

== Registering av representasjonsforhold

Tjeneste for å registrere et representasjonsforhold som gir leverandør mulighet til å opptre på vegne av konsument

== Delegerbar ressurs

Dataobjekt som beskriver en ressurs, f.eks. et API, som det kan gis rettigheter til gjennom et representasjonsforhold.

== Delegering av rettigheter til databehandler (leverandør)

Prosessen med å delegere rettigheter til databehandler/leverandør.

=== Inngå avtale med leverandør

Prosessen med å inngå en avtale med leverandør. En slik avtale vil normalt være inngått tidligere og uavhengig av om man skal ta i bruk et nytt API. En tjenesteavtale med leverandør er en forutsetning for å kunne delegere en tilgang.

=== Registrere delegert tilgang

Prosessen med å delegere tilganger. I tilknytning til eOppslag vil formålet være å gi leverandør tilgang til å representere konsument overfor et API, men registreringen vil potensielt også kunne gjelde for andre områder.

== Delegere rettigheter til databehandler

Evnen til å delegere rettigheter til databehandler som utfører oppgaver på vegne av behandlingsansvarlig.

== Altinn autorisasjon

Altinn autorisasjon gir teknisk samhandlingsevne ved at bruker logger inn med elektronisk ID,
autorisasjonskomponenten sjekker at bruker har de tilgangene som kreves for å ta i bruk aktuell tjeneste.

Dersom en privatperson skal ta i bruk en tjeneste på vegne av en virksomhet, gjøres det et oppslag mot Enhetsregisteret for å bekrefte at personen har en rolle på vegne av virksomheten. Dette gir organisatorisk samhandlingsevne.

Mer om Altinn autorisasjon:
https://www.altinndigital.no/produkter/styring-av-tilgang/


== Samhandlingsaktør

Samlebetegnelse på roller som inngår i en samhandlingsprosess og samhandler med en annen samhandlingsaktør. Kan være en tilbyder, konsument, avsender, mottaker, leverandør etc.

