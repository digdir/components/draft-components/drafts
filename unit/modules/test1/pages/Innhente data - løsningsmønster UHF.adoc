= Innhente data - løsningsmønster UHF
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagesdir: ../images
endif::[]
:toc: left
:toclevels: 7
:sectnums:
:sectnumlevels: 7

== Oversikt



.Innhente data - løsningsmønster UHF
image::Innhente data - løsningsmønster UHF.png[alt=Innhente data - løsningsmønster UHF image]

== Maskinporten

Maskinporten sørger for sikker autentisering og tilgangskontroll for datautveksling mellom
virksomheter, maskin til maskin. Dette gir teknisk og organisatorisk samhandlingsevne.

Mer om Maskinporten:
https://www.digdir.no/digitale-felleslosninger/maskinporten/869

== Data-konsument

Den som innhenter eller mottar data fra andre aktører.

== Tokentjeneste

Tjeneste som utsteder sikkerhetsbilletter. Sikkerhetsbillett utstedes basert på tildelte rettigheter og eventuelle representasjonsforhold.

== Virksomhetssertifikat

En virksomhets elektroniske ID. Benyttes for å autentisere virksomheten overfor tokentjenesten.

== Innhente data

Evnen til å innhente data fra en annen aktør.

== Endepunkt (adresse)

Dataobjekt som representerer teknisk adresse til et API eller ressurs.

== Oppslag representasjons-forhold

Tjeneste som benyttes av tokentjenesten for å kontrollere om det foreligger et delegert representasjonsforhold fra konsument til leverendør i autorisasjonstjenesten til Altinn.

== Slå opp data gjennom et API 

Prosessen med å slå opp og hente data gjennom et API.

=== Utfør tjenestekall

Prosessen med å benytte (gjøre et oppslag mot) et eksternt API.

=== Innhent samtykke ved behov

Prosess for å innhente samtykke fra person eller virksomhet som grunnlag for å innhente data. Dette gjøres kun ved behov.

=== Hent teknisk endepunkt ved behov

Prosessen å slå opp den tekniske adressen til et API før spørring mot API-et. Gjøres kun dersom det er nødvendig.

=== Be om tilgangstoken

Prosessen med å benytte en sikkerhetsbillettjeneste for hente en sikkerhetsbillett som gir tilgang til et API. Dette forutsetter at alt er registert og satt opp riktig mot de aktuelle tjenestene.

== Tilganger konsument

Oversikt over hvilke API og OAUTH-scopes en virksomhet (representert ved organisasjonsnummer) skal ha tilgang til (utstedt token for).

== Altinn autorisasjon

Altinn autorisasjon gir teknisk samhandlingsevne ved at bruker logger inn med elektronisk ID,
autorisasjonskomponenten sjekker at bruker har de tilgangene som kreves for å ta i bruk aktuell tjeneste.

Dersom en privatperson skal ta i bruk en tjeneste på vegne av en virksomhet, gjøres det et oppslag mot Enhetsregisteret for å bekrefte at personen har en rolle på vegne av virksomheten. Dette gir organisatorisk samhandlingsevne.

Mer om Altinn autorisasjon:
https://www.altinndigital.no/produkter/styring-av-tilgang/


== Felles API-katalog

Del av Felles datakatalog som gir mulighet for å søke etter API-er og lese API-spesifikasjoner https://fellesdatakatalog.brreg.no/apis

== Adressetjeneste

Tjeneste som gir mulighet til å slå opp teknisk endepunkt

== Datautvekslings-tjeneste

Tjeneste for utveksling av data over aktuell transportprotokoll, f.eks, gjennom asynkrone medlinger eller synkrone API-oppslag.

== Samtykkeregistererings-tjeneste

Tjeneste for å innhente samtykke fra den registrert som dataene gjelder. Dette kan være en person eller en virksomhet.

== Altinn autorisasjon

Altinn autorisasjon gir teknisk samhandlingsevne ved at bruker logger inn med elektronisk ID,
autorisasjonskomponenten sjekker at bruker har de tilgangene som kreves for å ta i bruk aktuell tjeneste.

Dersom en privatperson skal ta i bruk en tjeneste på vegne av en virksomhet, gjøres det et oppslag mot Enhetsregisteret for å bekrefte at personen har en rolle på vegne av virksomheten. Dette gir organisatorisk samhandlingsevne.

Mer om Altinn autorisasjon:
https://www.altinndigital.no/produkter/styring-av-tilgang/


== API Gateway hos dataeier / OAuth resource server

Komponent som beskytter ressursene og monitorerer tilgang inklusivt:

*  Validere tilgangstoken
*  Beskytte mot inntrenging og andre trusler
• Håndtere volumbegrensninger og andre abonnementsordninger
• Håndheve tilgangsstyring
• Samle inn data om bruken av API-er
• [orkestrering mellom interntjeneser??]

Prinsipp fra Steiner:
Oppgaver både sikkerhetsperspektiv og forvaltningsperspektivet
Dersom gatway skal stå for sikkerheten, MÅ VÆRE SIKKER PÅ at API ikke kan nås utenom gateway
Noen må passe VELDIG godt på gatewayen og holder den oppdatert.

Det må være et tydeligvalg om hvem (gateway eller API) har ansvar for sikkerhetsmessig.


== UHF Oauth2 autorisasjonstjener

Autorisasjonstjeneter realiserer tilgangskontroll gjennom å utstede OpenID Connect eller OAuth 2 grants og tokens. En autorisasjonstjener er også brukt til å realisere tilgangs policy. 

Scope (rettighet) og Audience (identifikator eller ressurs-server f. eks. FS)

Per i dag er Scopes er grovkornet
Rich authorization requsts (ny spesifikasjon) for å utvide scope begrep i IETF
underenhet og behandlingssted, behandlingsgrunnlag og roller jobbes det med.

== UHF API katalog

Registeret skal være mest mulig selvbetjent

