= Innledning
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
include::architecture-repository:common:partial$commonincludes.adoc[]
:imagepath: master@drafts:data-exchange-strategy-introduction:
endif::[]
:toc:
:toclevels: 7
:sectnums:
:sectnumlevels: 9

image:architecture-repository:common:under-construction.png[width=45]

Hva slags veiledning trenger virksomhetene for å navigere i dagens landskap av løsninger for datautveksling?
Hva bør på plass av målbilder og referansearkitekturer for å gi retning til arbeidet med fellesløsninger for datautveksling?  
Hva slags strategi behøves for videre arbeid med nasjonal infrastruktur for datautveksling?
Hva menes med nasjonal infrastruktur for datautveksling?


Gammel tekst:

Formålet med "notatet" er å gi innspill til arbeid med, og samarbeid om,
nasjonale målbilder og praktisk veiledning til virksomhetene, nærmere
bestemt:

* Innspill til arbeid med målbilder, referansearkitekturer og veikart
for nasjonal digital infrastruktur, med innebygget interoperabilitet
mellom sektorer og organisasjoner som visjon.
* Innspill til arbeid med praktisk veiledning for virksomhetene rundt
valg av mønstre og løsninger for datautveksling, slik at det treffer
behovet i dagens situasjon.
* Invitasjon til forvaltningen om samarbeid og påvirkning i forkant av
viktige og nærstående veivalg i utviklingen av Digdirs fellesløsninger.

Primær målgruppe er virksomhetsarkitekter og løsningsarkitekter i
offentlig sektor, samt produktledelse for fellesløsninger i Digdir og
«felles økosystem» (flere, på tvers av sektorene, inkl. privat sektor).

WARNING: __Her er det mange temaer i spill og gode
muligheter for «information overflow». Vi behøver helt klart å jobbe mer
med innhold, framstilling og navigering for ulike målgrupper – men dette
notatet er i denne omgang som nevnt et [.underline]#midlertidig
diskusjonsunderlag#, som er tatt fram på kort varsel og etter beste evne.__



