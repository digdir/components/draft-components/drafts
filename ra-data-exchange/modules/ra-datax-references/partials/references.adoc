
:Enterprise-Integration-Patterns: https://learning.oreilly.com/library/view/enterprise-integration-patterns/0321200683/[Enterprise Integration Patterns: Designing, Building, and Deploying Messaging Solutions]

:Australian-2015-report-on-messaging: https://www.sbr.gov.au/sites/default/files/Message-Protocols-for-Enabling-Digital-Government-Services-Final-Report.pdf?acsf_files_redirect[Message Protocols for Enabling Digital Services: A Report for the Australian Government, June 2015]

:Common-message-exchange-and-routing-patterns-ref-RedHat: https://www.redhat.com/architect/architectural-messaging-patterns[Architectural messaging patterns: an illustrated guide (RedHat 2021)]

:Difference-Between-Synchronous-and-Asynchronous-Messages: https://www.researchgate.net/publication/339124293_Difference_Between_Synchronous_and_Asynchronous_Messages_Synchronous_Messages[Difference Between Synchronous and Asynchronous Messages]

:Marrying-RESTful-HTTP-with-Asynchronous-and-Event-Driven-Services: https://sookocheff.com/post/api/marrying-restful-http-with-asynchronous-design/[Marrying RESTful HTTP with Asynchronous and Event-Driven Services (Kevin Sookocheff)]

:Message-queuing-terminology-ibm-mq: https://www.ibm.com/docs/en/ibm-mq/8.0?topic=queuing-message-terminology[Message queuing terminology (IBM MQ)]

:Asynchronous-Messaging-Patterns-Mulesoft: https://blogs.mulesoft.com/api-integration/patterns/asynchronous-messaging-patterns/[Asynchronous Messaging Patterns (Mulesoft)]

:Queues-vs-Topics-vs-Virtual-Topics-ActiveMQ: https://tuhrig.de/queues-vs-topics-vs-virtual-topics-in-activemq/[Queues vs. Topics vs. Virtual Topics (in ActiveMQ)]

:Microservice-Architecture-microservices-io: https://microservices.io/patterns/microservices.html[Pattern: Microservice Architecture (microservices.io)]	

:Features-and-terminology-in-Azure-Event-Hubs: https://docs.microsoft.com/en-us/azure/event-hubs/event-hubs-features[Features and terminology in Azure Event Hubs]

:OSI-modellen: https://no.wikipedia.org/wiki/OSI-modellen[OSI-modellen]

:IDS-RAM: https://internationaldataspaces.org/wp-content/uploads/IDS-Reference-Architecture-Model-3.0-2019.pdf[International Data Spaces Reference Architecture Model (2019)]

:Gaia-X-architecture-document: https://www.gaia-x.eu/sites/default/files/2021-05/Gaia-X_Architecture_Document_2103.pdf[Gaia-X Architecture Document]


:OPENDEI-RAF: https://www.opendei.eu/wp-content/uploads/2020/10/D2.1-REF-ARCH-FOR-CROSS-DOMAIN-DT-V1_UPDATED.pdf[OPENDEI D2.1 REFERENCE ARCHITECTURE FOR  CROSS-DOMAIN DIGITAL TRANSFORMATION]

:Målarkitektur-for-datadeling-eHelse: https://www.ehelse.no/standarder/malarkitektur-for-datadeling-i-helse-og-omsorgssektoren/_/attachment/inline/a5a908cd-5054-4d21-8eaf-8b795dcb25ea:761793d5dd6b6a1f2b9dd334a44e3a754d5b88e6/M%C3%A5larkitektur%20for%20datadeling%20i%20helse-%20og%20omsorgssektoren%20(HITR%201231_2021).pdf[Målarkitektur for datadeling i helse-og omsorgssektoren, versjon 1.0]


