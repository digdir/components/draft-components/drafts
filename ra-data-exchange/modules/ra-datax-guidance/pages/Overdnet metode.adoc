= Overdnet metode
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: master@drafts:ra-datax-guidance:
endif::[]
:toc: left
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 9

Steg i overordnet metode:

1. Start med å kartlegge brukerreisene for aktuelle sluttbrukere, samt de tjenestene som behøves for å realisere brukerreisene.

2. Design tverrgående forretningsprosesser (tjenestekjeder). Gjennom dette ses dataflyten i forretningsprosessene, dvs. behovet for datautveksling og krav til tjenestekvalitet m.m.

3. Avklar eventuelle juriske problemstillinger i tilknytning til datautveksling, f.eks. ved utveksling av personopplysninger.

4. Definer meldingstyper og formater og inngå nødvendige avtaler, enten bilateralt eller gjennom et avtalefelleskap (segmentansvarlig).

5. Bestem samhandlingsmønster (organisatorisk) og utvekslingsmønster (teknisk) for hvert enkelt integrasjonspunkt ut fra det forretningsmessige behovet for samhandling.
+
NOTE: Det forretningsmessige behovet bør finnes i en oversikt over generiske brukstilfeller, og det bør finnes en mapping fra brukstille til samhandlingsmønster og utvekslingsmønster.

6. Velg blant eksisterende tekniske løsninger ut fra kvalitetsegenskaper m.m.., eller om nødvendig utvikle nye løsninger. 

.Illustrasjon av konseptet for valg av mønster og løsning
image::{imagepath}EIF-navigation.png[]


