= Tilnærming
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: master@drafts:ra-datax-introduction:
endif::[]
:toc: left
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 9

Det bygges videre på tidligere struktur med tre hovedtyper utvekslingsmønstre, dvs. forsendelse, forespørsel-svar og publisering av hendelsesdata. 

For å samsvare bedre med slik det er gjort i eHelse, etableres i tillegg et sett av mer forretningsorienterte samhandlingsmønstre.

Det etableres også et begrepsapparat som bygger bro mellom ulike bruk av viktige begreper som f.eks. datadeling, dokumentdeling, meldingsutveksling og dokumentutveksling.

//Dokumentstrukturen legger opp til at omfanget kan utvides til å dekke mer enn bare en overordnet beskrivelse av datautveksling, slik som veiledning til valg mellom mønstre. Deler av innholdet bør dessuten kunne integreres med eksisterende beskrivelser av referansearkitekturene.


