= Bakgrunn
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: master@drafts:ra-datax-introduction:
endif::[]
:toc: left
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 9

Lærebøker, artikler og blogger fra akademia, produktleverandører og frittstående forfattere gir ulike framstillinger av konsepter, begreper og mønstre for datautveksling.

Nasjonale referansearkitekturer for datautveksling er i dag bygd opp rundt en struktur med tre typer utvekslingsmønstre, dvs. meldingsforsendelse, forespørsel-svar og publisering av hendelsesdata. 

I parallell ned utviklingen av dette har det foregått tilsvarende arbeid i noen av sektorene, bare delvis samordnet. 

Arkitektmiljøene i de ulike sektorene har etablert samarbeidsarenaer, men arbeider fremdeles med sektorielle arkitekturprodukter.

//Arbeidet med Referansearkitektur for deling av data i høyere utdanning og forsking har tatt utgangspunkt i de nasjonale referansearkitekturene og er rimelig godt koordinert. Selve definisjonen av begrepet datadeling er likevel ulik.

//I helsesektoren har ... 





