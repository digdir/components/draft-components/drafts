= eHelse samhandlingsformer
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: master@drafts:ra-datax-compare:
endif::[]
:toc: left
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 9



.eHelse samhandlingsformer
image::{imagepath}eHelse samhandlingsformer.png[alt=eHelse samhandlingsformer image]


****
xref:master@drafts:ra-datax-compare:page$eHelse samhandlingsformer.var.1.adoc[Vis detaljer om elementene i diagrammet] (Tips: kbd:[Shift]-klikk for å åpne i nytt vindu)
****


