= Map nasjonale samhandlingsmønstre mot eHelse
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: master@drafts:ra-datax-compare:
endif::[]
:toc: left
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 9



.Map nasjonale samhandlingsmønstre mot eHelse
image::{imagepath}Map nasjonale samhandlingsmønstre mot eHelse.png[alt=Map nasjonale samhandlingsmønstre mot eHelse image]


TIP: Gå tilbake til standardvisning ved å klikke 'tilbake' (eller lukk dette vinduet om det er nytt)


[cols ="1,3", options="header"]
.Elementer i view for Map nasjonale samhandlingsmønstre mot eHelse
|===

| Element
| Beskrivelse

| Plattformen for helhelhetlig samhandling vil tilby 3 samhandlingsformer:
a| 

| Endre og dele (eHelse)
a| Den autoritative kilden ligger i samhandlingsløsningen(e) i et behandlingsrettet register. Journalløsningen kan registrere og dele informasjonen med andre aktører, og andre aktører kan registrere og dele informasjonen i samhandlingsløsningen(e).

| Slå opp og tilgjengeliggjøre (eHelse)
a| Dette er relevante dokumenter (dokumentdeling) eller strukturerte opplysninger (datadeling) som kan søkes opp og tilgjengeliggjøres fra en journalløsning til en annen.

| Sende og motta (eHelse)
a| Dette er helseopplysninger som utveksles mellom en sender og en mottaker som er kjente for hverandre.

| Meldingsforsendelse
a| _Meldingsforsendelse_ benyttes her som begrep på enhver form for 

| Meldingsutveksling
a| 

| Publisering av notifikasjoner om hendelser
a| 

| Spørring og svar på spørring
a| 

| Oppdatering av data i annen løsning
a| 

| Oversendelse av signert datadistribusjon (dokument)
a| 

| Oppslag i datakilde
a| 

| Oppdatering av data i annen løsning
a| 

| eHelse samhandlingsmodeller (mønstre)
a| 

| Direkte tilgang (eHelse)
a| Ref. https://www.ehelse.no/standarder/ikke-standarder/samhandlingsarkitekturer-i-helse-og-omsorgssektoren/_/attachment/inline/f6ad8201-ddfb-4115-90c8-141cbe4623ce:0dcd135982875fa7e205260be707301a691d42a8/Samhandlingsarkitekturer%20i%20helse-%20og%20omsorgssektoren.pdf[Samhandlingsarkitekturer i helse- og omsorgssektoren]:

Direkte tilgang (Innsyn) til fellesløsning eller løsning i en annen virksomhet.

| Samhandling i felles kjernesystem (eHelse)
a| 

| Meldingsutveksling (eHelse)
a| Overføring av strukturerte data til kjent mottaker (som en del av en automatisk prosessering. 

Ref. https://www.ehelse.no/standarder/ikke-standarder/samhandlingsarkitekturer-i-helse-og-omsorgssektoren/_/attachment/inline/f6ad8201-ddfb-4115-90c8-141cbe4623ce:0dcd135982875fa7e205260be707301a691d42a8/Samhandlingsarkitekturer%20i%20helse-%20og%20omsorgssektoren.pdf[Samhandlingsarkitekturer i helse- og omsorgssektoren]

| Dokumentutveksling (eHelse)
a| Overføring av godkjent, lesbart dokument, med varierende grad av struktur. 

Merk: I Norge har vi ofte omtalt dokumentutveksling som meldingsutveksling, og en elektronisk melding kan derfor dekke både meldingsutveksling og dokumentutveksling

Ref. https://www.ehelse.no/standarder/ikke-standarder/samhandlingsarkitekturer-i-helse-og-omsorgssektoren/_/attachment/inline/f6ad8201-ddfb-4115-90c8-141cbe4623ce:0dcd135982875fa7e205260be707301a691d42a8/Samhandlingsarkitekturer%20i%20helse-%20og%20omsorgssektoren.pdf[Samhandlingsarkitekturer i helse- og omsorgssektoren]

| Datadeling (eHelse)
a| Ref. https://www.ehelse.no/standarder/ikke-standarder/samhandlingsarkitekturer-i-helse-og-omsorgssektoren/_/attachment/inline/f6ad8201-ddfb-4115-90c8-141cbe4623ce:0dcd135982875fa7e205260be707301a691d42a8/Samhandlingsarkitekturer%20i%20helse-%20og%20omsorgssektoren.pdf[Samhandlingsarkitekturer i helse- og omsorgssektoren]:

Deling av strukturerte data gjennom felles ressurser/tjenester 


Ref. https://www.ehelse.no/standarder/malarkitektur-for-datadeling-i-helse-og-omsorgssektoren/_/attachment/inline/a5a908cd-5054-4d21-8eaf-8b795dcb25ea:761793d5dd6b6a1f2b9dd334a44e3a754d5b88e6/M%C3%A5larkitektur%20for%20datadeling%20i%20helse-%20og%20omsorgssektoren%20(HITR%201231_2021).pdf[Målarkitektur for datadeling i helse- og omsorgssektoren]:

Datadeling er en samhandlingsmodell i helsesektoren hvor strukturerte data om en pasient deles i sanntid på tvers av virksomheter og med innbygger i helsesektoren.

Når vi i målarkitekturen snakker om datadeling, så mener vi alle behovene for samhandling som kan løses ved å dele strukturerte data om en pasient, både muligheten for å lese, behandle og oppdatere data via datadeling.

I dette dokumentet omtales API-er som API-er som kan nås via http(s) og som kan benyttes av andre enn virksomheten som eier API-et. Direktoratet for e-helse har valgt å benytte begrepet "åpne API" for å etablere en felles forståelse og forventninger til hverandre for et API som kan tas i bruk av en annen aktør. Åpne API må ikke forveksles med åpne data, da helseopplysningene som tilbys gjennom åpne API må sikres for å ivareta krav til informasjonssikkerhet og personvern.

Nasjonal e-helsetjeneste slik som Kjernejournal og Reseptformidleren benyttes som samhandlingsløsninger hvor en av formålene er å unngå mange til mange samhandlingsformer


| Dokumentdeling (eHelse)
a| Deling av godkjent, lesbart dokument gjennomfelles infrastruktur/tjenester med andre virksomheter.

Et dokument kan være alt fra et bilde til et strukturert dokument. Helsepersonell må signere journaldokumenter når de er ferdige. Dokumentdeling baserer seg på deling av endelige dokumenter


Ref. https://www.ehelse.no/standarder/ikke-standarder/samhandlingsarkitekturer-i-helse-og-omsorgssektoren/_/attachment/inline/f6ad8201-ddfb-4115-90c8-141cbe4623ce:0dcd135982875fa7e205260be707301a691d42a8/Samhandlingsarkitekturer%20i%20helse-%20og%20omsorgssektoren.pdf[Samhandlingsarkitekturer i helse- og omsorgssektoren]

|===
****
TIP: Gå tilbake til standardvisning ved å klikke 'tilbake' (eller lukk dette vinduet om det er nytt)
****


