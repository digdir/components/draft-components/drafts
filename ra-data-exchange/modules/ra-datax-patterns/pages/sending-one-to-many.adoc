= Forsendelse - en til flere kjente
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: master@drafts:ra-datax-patterns:
endif::[]
:toc: left
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 9

Forsendelse fra en til flere kjente bygger på _basismønsteret for forsendelse fra en til en_, ved at samme innhold sendes gjennom gjentatte, men separate meldinger til et antall kjente mottakere.

Brukstilfeller (eksempler):

* Begrenset høring
* Igangsetting av flere parallelle jobber

.Forsendelse - en til flere kjente
image::{imagepath}Forsendelse - en til flere kjente.png[alt=Forsendelse - en til flere kjente image]




