= Forespørsel-svar - en mot flere kjente
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: master@drafts:ra-datax-patterns:
endif::[]
:toc: left
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 9

Dette mønsteret bygger på basismønsteret for forespørsel-svar. Samme forespørsel sendes mot flere kjente tilbydere og mottatte svar prosesseres ut fra valgt strategi. F.eks. kan strategien være at første svar bestandig velges, eller f.eks. kan alle svar kombineres til et samlet svar.

.Forespørsel-svar - en mot flere kjente
image::{imagepath}Forespørsel-svar - en mot flere kjente.png[alt=Forespørsel-svar - en mot flere kjente image]




