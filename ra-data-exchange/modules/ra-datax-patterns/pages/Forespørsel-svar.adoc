= Forespørsel-svar
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: master@drafts:ra-datax-patterns:
endif::[]
:toc: left
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 9



:leveloffset: +1
include::master@drafts:ra-datax-patterns:page$request-reply-basic.adoc[]
include::master@drafts:ra-datax-patterns:page$request-reply-many-known.adoc[]
include::master@drafts:ra-datax-patterns:page$request-reply-many-unknown.adoc[]
:leveloffset: -1
