= Publisering flere til flere ukjente via en hendelsesliste
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: master@drafts:ra-datax-patterns:
endif::[]
:toc: left
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 9

Flere kan skrive mot samme hendelsesliste. som kan leses av interesserte konsumenter.

.Publisering flere til flere ukjente via en hendelsesliste
image::{imagepath}Publisering flere til flere ukjente via en hendelsesliste.png[alt=Publisering flere til flere ukjente via en hendelsesliste image]




