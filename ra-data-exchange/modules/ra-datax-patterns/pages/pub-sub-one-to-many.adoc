= Publisering fra en til flere ukjente abonnementer (pub-sub)
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: master@drafts:ra-datax-patterns:
endif::[]
:toc: left
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 9

Publisert meldingsinnhold kan leses av alle interesserte abonnenter på det aktuelle emnet, så lenge innholdet er tilgjengelig (tidsbegrenset eller andre kriterier for gyldighet).

.Publisering fra en til flere ukjente abonnementer (pub-sub)
image::{imagepath}Publisering fra en til flere ukjente abonnementer (pub-sub).png[alt=Publisering fra en til flere ukjente abonnementer (pub-sub) image]




