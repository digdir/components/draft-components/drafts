= Generiske samhandlingsmønstre
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: master@drafts:patterns-use-cases:
endif::[]
:toc: left
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 9



.Generiske samhandlingsmønstre
image::{imagepath}Generiske samhandlingsmønstre.png[alt=Generiske samhandlingsmønstre image]




